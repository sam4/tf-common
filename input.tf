variable "stage" {}

variable "address_space" {
    type = list(string)
}
variable "address_prefixes" {}

variable "tfstate_applications" {}



variable "crmmis_subnet_IP4" {}
variable "crmmis_api_key" {}
variable "instrumentorderform_subnet_IP4" {}
