variable "stage" {
    type = string
}

variable "location" {
    default = "West Europe"
}

variable "subnet_IP4" {
    type = list
}
