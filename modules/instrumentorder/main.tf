
#======================================== START NETWORK SETUP =================================

data "azurerm_resource_group" "networkrg" {
  name = "${var.stage}-we-network-01-rg"
}

data "azurerm_virtual_network" "sharedvnet" {
  name                = "${var.stage}-we-core-01-vnet"
  resource_group_name = data.azurerm_resource_group.networkrg.name
}

resource "azurerm_subnet" "subnet" {
    name                    = "${var.stage}-we-core01instrumentorderform-01-sub"
    resource_group_name     = data.azurerm_resource_group.networkrg.name
    virtual_network_name    = data.azurerm_virtual_network.sharedvnet.name
    address_prefixes          = var.subnet_IP4

    delegation {
        name = "instrumentorderform_subnet_delegation"

        service_delegation {
            name    = "Microsoft.Web/serverFarms"
            actions = [
                "Microsoft.Network/virtualNetworks/subnets/action"
            ]
        }
    }

}

resource "azurerm_network_security_group" "appnsg" {
  name                = "${var.stage}-we-core01instrumentorderform-01-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet_network_security_group_association" "nsgassoc" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.appnsg.id
}

#======================================== END NETWORK SETUP =================================

resource "azurerm_resource_group" "rg" {
    name     = "${var.stage}-we-instrumentorder-01-rg"
    tags     = {
        "Department" = "AM_AXS"
    }
    location = var.location
}



resource "azurerm_app_service_plan" "serviceplan_linux" {
    name                = "${var.stage}-we-instrumentorder-01-planl"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name

    kind = "linux"
    maximum_elastic_worker_count = 1

    #TODO clarify why?
    reserved = true

    sku {
        tier = "Standard"
        size = "S2"
    }
}


resource "azurerm_app_service" "instrumentorder" {
    name                = "${var.stage}-we-instrumentorder-02-app"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name
    app_service_plan_id = azurerm_app_service_plan.serviceplan_linux.id

    site_config {
        dotnet_framework_version = "v4.0"
        scm_type                 = "None"

        default_documents         = []

    }
}

resource "azurerm_app_service" "instrumentorderapi" {
    name                = "${var.stage}-we-instrumentorderapi-02-app"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name
    app_service_plan_id = azurerm_app_service_plan.serviceplan_linux.id

    site_config {
        dotnet_framework_version = "v4.0"
        scm_type                 = "None"

        default_documents         = []

    }
}

resource "azurerm_app_service_virtual_network_swift_connection" "vnetintegration_instrumentorderapi" {
  app_service_id = azurerm_app_service.instrumentorderapi.id
  subnet_id      = azurerm_subnet.subnet.id
}

resource "azurerm_sql_server" "sqlserver" {
    name                         = "${var.stage}-we-instrumentorder-02-dbserver"
    resource_group_name          = azurerm_resource_group.rg.name
    location                     = var.location
    version                      = "12.0"
    administrator_login          = "dbAdmin"
    #TODO align with real one
    administrator_login_password = "4-v3ry-53cr37-p455w0rd"
}

resource "azurerm_sql_database" "master" {
    name                = "master"
    resource_group_name = azurerm_resource_group.rg.name
    location            = var.location
    server_name         = azurerm_sql_server.sqlserver.name

}

resource "azurerm_sql_database" "db" {
    name                = "${var.stage}-we-instrumentorder-02-db"
    resource_group_name = azurerm_resource_group.rg.name
    location            = var.location
    server_name         = azurerm_sql_server.sqlserver.name

}

resource "azurerm_application_insights" "insights" {
    name                       = "${var.stage}-we-instrumentorder-01-ais"
    location                   = var.location
    resource_group_name        = azurerm_resource_group.rg.name
    application_type           = "web"
    retention_in_days          = 90
    sampling_percentage        = 0
}


