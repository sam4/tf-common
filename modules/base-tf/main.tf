#The purpose of this module is to create the storage account and the containers for creating tfstate files

resource "azurerm_resource_group" "rg" {
    name     = "${var.stage}-we-terraform-01-rg"
    location = var.location
}

resource "azurerm_storage_account" "storage" {
    name                     = "${var.stage}wetfstate01sas"
    resource_group_name      = azurerm_resource_group.rg.name
    location                 = azurerm_resource_group.rg.location
    account_tier             = "Standard"
    account_replication_type = "GRS"
}

resource "azurerm_storage_container" "tfbase" {
    name                  = "tfbase"
    storage_account_name  = azurerm_storage_account.storage.name
    #container_access_type = "private"
}

#================================== tfbase for all applications =====================================
#the following is executed for every application

# Query the Principal ID for each user in support_users (creation is done in the onboarding as can currently not be done by terraform)
#get the user
#data "azuread_user" "tfbase_user" {
#  user_principal_name = "${var.stage}-upn1"
#}

resource "azurerm_storage_container" "tfstate_app_container" {
    for_each              = toset(var.applications)
    name                  = each.key
    storage_account_name  = azurerm_storage_account.storage.name
    #container_access_type = "private"
}



#create the role for access to the tfstate container (bitbucket deployments will need this)
#resource "azurerm_role_definition" "tfbase_role" {
#  name        = "bitbucket_tfbase"
#  scope       = var.subscription_id
#  description = "Custom role for accessing tfstate of application tfbase"
#  permissions {
#    actions = []
#    not_actions = []
#  }
#  assignable_scopes = [
#    #azurerm_dashboard.insights-dashboard.id,
#  ]
#}

#assign user
#resource "azurerm_role_assignment" "tfbase_roleassignement" {
#  scope              = var.subscription_id
#  role_definition_id = azurerm_role_definition.tfbase_role.id
#  principal_id       = data.azuread_user.tfbase_user.object_id
#}

#================================== app1 =====================================

#resource "azurerm_storage_container" "crmmis" {
#    name                  = "crmmis"
#    storage_account_name  = azurerm_storage_account.storage.name
#    #container_access_type = "private"
#}
