variable "stage" {
    type = string
}

variable "location" {
    default = "West Europe"
}

variable "applications" {
    description = "List of applications that will use Terraform"
    type = list(string)
}
