
resource "azurerm_resource_group" "rg" {
    name        = "${var.stage}-we-architecture-01-rg"
    location    = var.location
}

#the creation of this might take some time
resource "azurerm_api_management" "api" {
    name                = "${var.stage}-we-slam-02-api"
    location            = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name
    publisher_name      = "Swiss Life Asset Managers"
    publisher_email     = "amsupport@swisslife.ch"

    sku_name = "Basic_1"

}
