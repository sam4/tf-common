variable "location" {
    type    = string
    default = "West Europe"
}

variable "stage" {
    type = string
}

variable "address_space" {
    type = list(string)
}

variable "address_prefixes" {
    type = list
}

variable "dns_servers" {
    default = ["10.41.150.20"]
}

