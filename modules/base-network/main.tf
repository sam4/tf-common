variable "default_tags" {
    default = {
        "Department" = "AM_AXS"
    }
}

resource "azurerm_resource_group" "rg" {
    name        = "${var.stage}-we-network-01-rg"
    tags        = var.default_tags
    location    = var.location
}

resource "azurerm_virtual_network" "core-vnet" {
    name                = "${var.stage}-we-core-01-vnet"
    address_space       = var.address_space
    location            = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name

    dns_servers         = var.dns_servers
}

resource "azurerm_subnet" "int" {
    name                    = "${var.stage}-we-core01int-01-sub"
    resource_group_name     = azurerm_resource_group.rg.name
    virtual_network_name    = azurerm_virtual_network.core-vnet.name
    address_prefixes          = var.address_prefixes
}

#resource "azurerm_subnet" "waf" {
#    name                    = "${var.stage}-we-core01waf-01-sub"
#    resource_group_name     = azurerm_resource_group.rg.name
#    virtual_network_name    = azurerm_virtual_network.core-vnet.name
#    address_prefixes          = "10.162.101.224/28" #TODO verify this is the same for all 3 stages?
#}

resource "azurerm_network_interface" "networkinterface" {
    name                = "${var.stage}-core-01-vm892"
    location            = var.location
    #TODO why is this core and not network?
    resource_group_name = "${var.stage}-we-core-01-rg"

    ip_configuration {
        name                          = "ipconfig1"
        subnet_id                     = azurerm_subnet.int.id
        private_ip_address_allocation = "Dynamic"
    }
}

resource "azurerm_network_security_group" "int" {
  name                = "${var.stage}-we-core01int01-01-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

#does not exist on old P!
#resource "azurerm_network_security_group" "waf" {
#  name                = "${var.stage}-we-core01waf01-01-nsg"
#  location            = azurerm_resource_group.rg.location
#  resource_group_name = azurerm_resource_group.rg.name
#}

resource "azurerm_subnet_network_security_group_association" "int" {
  subnet_id                 = azurerm_subnet.int.id
  network_security_group_id = azurerm_network_security_group.int.id
}

#resource "azurerm_subnet_network_security_group_association" "waf" {
#  subnet_id                 = azurerm_subnet.waf.id
#  network_security_group_id = azurerm_network_security_group.waf.id
#}
