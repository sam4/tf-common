
#======================================== START NETWORK SETUP =================================

data "azurerm_resource_group" "networkrg" {
  name = "${var.stage}-we-network-01-rg"
}

data "azurerm_virtual_network" "sharedvnet" {
  name                = "${var.stage}-we-core-01-vnet"
  resource_group_name = data.azurerm_resource_group.networkrg.name
}

resource "azurerm_subnet" "subnet" {
    name                    = "${var.stage}-we-core01crmmis-01-sub"
    resource_group_name     = data.azurerm_resource_group.networkrg.name
    virtual_network_name    = data.azurerm_virtual_network.sharedvnet.name
    address_prefixes        = var.subnet_IP4

    delegation {
        name = "crmmis_subnet_delegation"

        service_delegation {
            name    = "Microsoft.Web/serverFarms"
            actions = [
                "Microsoft.Network/virtualNetworks/subnets/action"
            ]
        }
    }

}

resource "azurerm_network_security_group" "appnsg" {
  name                = "${var.stage}-we-core01crmmis-01-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet_network_security_group_association" "nsgassoc" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.appnsg.id
}

#======================================== END NETWORK SETUP =================================

resource "azurerm_resource_group" "rg" {
    name     = "${var.stage}-we-crmmis-01-rg"
    tags     = {
        "Department" = "AM_AXS"
    }
    location = var.location
}

resource "azurerm_resource_group" "rg_linux" {
    name     = "${var.stage}-we-crmmis-02-rg"
    tags     = {
        "Department" = "AM_AXS"
    }
    location = var.location
}


resource "azurerm_app_service_plan" "serviceplan_linux" {
    name                = "${var.stage}-we-crmmis-01-planl"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg_linux.name

    kind = "linux"
    maximum_elastic_worker_count = 1

    #TODO clarify why?
    reserved = true

    sku {
        tier = "Standard"
        size = "S1"
    }
}

resource "azurerm_app_service_plan" "serviceplan_windows" {
    name                = "${var.stage}-we-crmmis-02-planw"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name

    kind = "app"
    maximum_elastic_worker_count = 1

    sku {
        capacity = 1
        tier = "Standard"
        size = "S1"
    }
}


resource "azurerm_app_service" "dynamics" {
    name                = "${var.stage}-we-crmdynamics-02-app"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name
    app_service_plan_id = azurerm_app_service_plan.serviceplan_windows.id

    site_config {
        dotnet_framework_version = "v4.0"
        scm_type                 = "None"

        always_on                 = true
        default_documents         = [
            "Default.htm",
            "Default.html",
            "Default.asp",
            "index.htm",
            "index.html",
            "iisstart.htm",
            "default.aspx",
            "index.php",
            "hostingstart.html",
        ]

        use_32_bit_worker_process = true
        php_version               = "5.6"

    }
}

resource "azurerm_app_service" "pitchapi" {
    name                = "${var.stage}-we-pitchapi-02-app"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name
    app_service_plan_id = azurerm_app_service_plan.serviceplan_linux.id

    site_config {
        dotnet_framework_version = "v4.0"
        scm_type                 = "None"

        always_on                 = true
        default_documents         = [
            "Default.htm",
            "Default.html",
            "Default.asp",
            "index.htm",
            "index.html",
            "iisstart.htm",
            "default.aspx",
            "index.php",
            "hostingstart.html",
        ]

        use_32_bit_worker_process = true
    }
}

resource "azurerm_app_service" "pitchcollector" {
    name                = "${var.stage}-we-pitchcollector-02-app"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name
    app_service_plan_id = azurerm_app_service_plan.serviceplan_linux.id

    site_config {
        dotnet_framework_version = "v4.0"
        scm_type                 = "None"

        always_on                 = true
        default_documents         = [
            "Default.htm",
            "Default.html",
            "Default.asp",
            "index.htm",
            "index.html",
            "iisstart.htm",
            "default.aspx",
            "index.php",
            "hostingstart.html",
        ]

        use_32_bit_worker_process = true
    }
}

resource "azurerm_app_service_virtual_network_swift_connection" "vnetintegration_dynamics" {
  app_service_id = azurerm_app_service.dynamics.id
  subnet_id      = azurerm_subnet.subnet.id
}

resource "azurerm_application_insights" "insights" {
    name                = "${var.stage}-we-crmmis-01-ais"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name
    application_type    = "web"
    retention_in_days   = 90
    sampling_percentage = 0

}

# NOTE: the Name used for Redis needs to be globally unique
#the creation of this may take up to 20m (or more)
resource "azurerm_redis_cache" "rediscache" {
    name                = "${var.stage}-we-crmmis-02-redis"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name
    capacity            = 0
    family              = "C"
    sku_name            = "Basic"
    enable_non_ssl_port = false
    minimum_tls_version = "1.2"

    redis_configuration {
    }
}

#get the api-management object (part of shared)
data "azurerm_api_management" "shared" {
  name                = "${var.stage}-we-slam-02-api"
  resource_group_name = "${var.stage}-we-architecture-01-rg"
}

resource "azurerm_api_management_api" "api" {
  name                = "${var.stage}-we-crmmis-01-api"
  resource_group_name = data.azurerm_api_management.shared.resource_group_name
  api_management_name = data.azurerm_api_management.shared.name
  revision            = "1"
  display_name        = "crmpitch API"
  path                = "example"
  protocols           = ["https"]
}


resource "azurerm_api_management_api_operation" "api_op" {
  operation_id        = "GET"
  api_name            = azurerm_api_management_api.api.name
  api_management_name = azurerm_api_management_api.api.api_management_name
  resource_group_name = azurerm_api_management_api.api.resource_group_name
  display_name        = "GET"
  method              = "GET"
  url_template        = "/*"
  description         = ""

}

resource "azurerm_api_management_named_value" "CRMMIS_API_KEY" {
  name                = "crmmis-api-key"
  resource_group_name = azurerm_api_management_api.api.resource_group_name
  api_management_name = azurerm_api_management_api.api.api_management_name
  display_name        = "CRMMIS_API_KEY"
  value               = var.api_key
}

resource "azurerm_api_management_api_operation_policy" "GET_policy" {
  api_name            = azurerm_api_management_api_operation.api_op.api_name
  api_management_name = azurerm_api_management_api_operation.api_op.api_management_name
  resource_group_name = azurerm_api_management_api_operation.api_op.resource_group_name
  operation_id        = azurerm_api_management_api_operation.api_op.operation_id

  xml_content = <<XML
<policies>
    <inbound>
        <base />
        <set-header name="X-API-KEY" exists-action="append">
            <value>{{CRMMIS_API_KEY}}</value>
        </set-header>
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
XML

}
