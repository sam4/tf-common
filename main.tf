# Configure the Azure Provider
provider "azurerm" {
    version = "~> 2.13"
    features {}
}

provider "azuread" {
    version = "~> 0.8"
}

data "azurerm_subscription" "current" {
}

terraform {
    backend "azurerm" {
        #this is configured via the -backend-config parameters on the init/plan/apply calls
    }
}

locals {
    subscription_id = data.azurerm_subscription.current.id
}



module "tfbase" {
  source = "./modules/base-tf"

  stage             = var.stage
  applications      = var.tfstate_applications
}


module "storage" {
  source = "./modules/base-storage"

  stage  = "${var.stage}"
}

module "network" {
  source = "./modules/base-network"

  stage             = var.stage
  address_space     = var.address_space
  address_prefixes    = var.address_prefixes
}

module "container-registry" {
  source = "./modules/container-registry"

  stage             = var.stage
}

module "api-management" {
  source = "./modules/api-management"

  stage             = var.stage
}

module "crmmis" {
    source = "./modules/crmmis"

    stage             = var.stage
    subnet_IP4        = var.crmmis_subnet_IP4
    api_key           = var.crmmis_api_key
}

module "instrumentorder" {
    source = "./modules/instrumentorder"

    stage             = var.stage
    subnet_IP4        = var.instrumentorderform_subnet_IP4
}
