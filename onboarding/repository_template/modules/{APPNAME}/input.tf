variable "stage" {
    type = string
}

variable "location" {
    default = "West Europe"
}

variable "subscription_id" {
    type = string
}
