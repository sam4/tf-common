resource_group_name   = "p-we-terraform-01-rg"
storage_account_name  = "pwetfstate01sas"
container_name        = "{APPNAME}"
key                   = "p-{APPNAME}.tfstate"
