resource_group_name   = "d-we-terraform-01-rg"
storage_account_name  = "dwetfstate01sas"
container_name        = "{APPNAME}"
key                   = "d-{APPNAME}.tfstate"
