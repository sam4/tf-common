# Configure the Azure Provider
provider "azurerm" {
    version = "~> 2.13"
    subscription_id = var.subscription_id #configured via the /config/<stage>.tfvars file
    features {}
}

terraform {
    backend "azurerm" {
        #this is configured via the -backend-config file in the /config/backend folder
    }
}

#usually you do not need to edit this unless you start having multiple modules or need more environment variables
#you should be coding you infrastructure as one (or multiple) modules. These are programmed via the main.tf in the
#modules folder
module "{APPNAME}_module" {
  source = "./modules/{APPNAME}"

  stage             = var.stage
  subscription_id   = var.subscription_id
}

