[Cmdletbinding()]
param(
    [Parameter(Position = 1)]
    [string]$ApplicationName = "crmmis",
    #[string]$RepositoryDestination,
    [string[]]$Stages = @("d","q","p")
)

$subscriptionmap = @{
    old_d = "2477e526-032d-467e-81cc-581f78914da8"
    old_p = "a36d7987-f343-477e-9af3-4cf67c224042"
    old_q = "237261f9-53a7-49ea-9226-58f5149a4ab4"
    d     = "13616073-9b41-46b4-9c75-dea64025c64a"
    p     = "ca37c358-9f8a-4a21-a8bb-11d49bc157a1"
    q     = "454360e5-4fff-4c6e-96f6-364a60a098ec"
}

#TODO: Create service principals
Write-Host "===================================================="
Write-Host "Creating Service Principals on Azure"
Write-Host "--------------------------"

Import-Module Az -Force -ErrorAction Stop

Connect-AzAccount -Tenant "3926ed3e-64fd-4bad-9591-0e1f547427ba"
$ServicePrincipals = @{}
foreach($s in $Stages) {
    $sub = Get-AzSubscription -SubscriptionId $subscriptionmap[$s]
    if($sub) {
        Set-AzContext $sub
        $SPName = "$s-tf-$ApplicationName"
        $ServicePrincipals[$s] = New-AzADServicePrincipal -DisplayName $SPName
    }
}

#Create the Repo in bitbucket
Write-Host "===================================================="
Write-Host "Setup of repository on bitbucket.org"
Write-Host "--------------------------"
Import-Module Atlassian.Bitbucket -ErrorAction Stop -Force #see https://github.com/beyondcomputing-org/Atlassian.Bitbucket

$login = Get-Bitbucketlogin

if (-not $login) {
    $bitbucket_creds = Get-Credential -Message "Please enter your bitbucket.org credentials"
    Login-Bitbucket -Credential $bitbucket_creds -Save
}

$repositories = Get-BitbucketRepository -ProjectKey TER

if($repositories.name -notcontains "tf-$ApplicationName") {
    $RepoParams = @{
        RepoSlug    = "tf-$ApplicationName"
        ProjectKey  = "TER"
        Private     = $true
    }
    New-BitbucketRepository @RepoParams
    #TODO set secrets on deployments
} else {
    Write-Warning "Found existing repository with name 'tf-$ApplicationName'"
}

#TODO clone to temp directory

#create the files of the repository
Write-Host "===================================================="
Write-Host "Creating new repository from templates"
Write-Host "--------------------------"

if($RepositoryDestination) {
    Write-Host "* Target: $RepositoryDestination"

    $replacements = @{
        AppName = $ApplicationName
    }
    Write-Host "Copying repository template to: $RepositoryDestination"
    $TemplateDir = Join-Path $PSScriptRoot -ChildPath "repository_template"
    $TemplateDirRegex = [Regex]::Escape($TemplateDir)
    $files =  Get-ChildItem $TemplateDir -File -Recurse

    foreach($f in $files) {
        Write-Host ("**" + ($f.FullName -replace $TemplateDirRegex,""))
        $content = Get-Content -Path $f.FullName -raw
        foreach($r in $replacements.GetEnumerator()) {
            $content = $content -replace "{$($r.key)}",$r.Value
        }
        $NewFullpath = $f.Fullname -replace $TemplateDirRegex, $RepositoryDestination
        New-Item $NewFullPath -Force -ItemType File | Out-Null
        Set-Content -Path $NewFullpath -Value $Content
    }
    #TODO commit to git/bitbucket
    Write-host "Please commit these changes to the new repository tf-$ApplicationName" -ForegroundColor Red
}
else {
    Write-host "No target repository path defined" -ForegroundColor Red
}



#Add to application list for tfbase module
Write-Host "===================================================="
Write-Host "Adding application to tfbase list"
Write-Host "--------------------------"
foreach($stage in $Stages) {

    $tfvarsfile         = Join-Path ((Get-Item $PSScriptroot).Parent.FullName) -ChildPath "config\$stage.tfvars"
    $ConfigContent      = Get-Content -Raw -Path $tfvarsfile

    #check if already in list
    if ($ConfigContent -notmatch "(tfstate_applications\s*=\s*\[.*\`"$ApplicationName\`".*])") {
        $ConfigAppendString = ",`"$ApplicationName`"]"
        $ConfigContent = $ConfigContent -replace "(tfstate_applications\s*=\s*\[\`".*)(\])", "`$1$ConfigAppendString"
        Set-Content -Path $tfvarsfile -Value $ConfigContent
        Write-Host "Added application to list of applications in ($tfvarsfile)"
    } else {
        Write-Warning "Application '$ApplicationName' already exists in the tfvars application list of: $tfvarsfile"
    }

}
Write-Host "Please commit, push, merge and deploy all config changes of the tf-common repository" -ForegroundColor Red

