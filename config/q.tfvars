stage = "q"
address_space = ["10.162.98.0/23"]
address_prefixes = ["10.162.98.0/24"]

tfstate_applications = ["crmmis","instrumentorderform"]


crmmis_subnet_IP4               = ["10.162.99.248/29"]
crmmis_api_key                  = "TODO. didnt have accessrights on old subscription"
instrumentorderform_subnet_IP4  = ["10.162.99.240/29"]
