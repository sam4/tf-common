stage = "p"
address_space = ["10.162.96.0/23"]
address_prefixes = ["10.162.96.0/24"]

tfstate_applications = ["crmmis","instrumentorderform"]


crmmis_subnet_IP4               = ["10.162.97.248/29"]
crmmis_api_key                  = "bbbcf85f-810b-4b57-8ba5-994bc545873d"
instrumentorderform_subnet_IP4  = ["10.162.97.240/29"]
