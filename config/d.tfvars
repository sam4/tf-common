stage = "d"
address_space = ["10.162.100.0/23"]
address_prefixes = ["10.162.100.0/24"]

tfstate_applications = ["crmmis","instrumentorderform"]

#put in own repositories
crmmis_subnet_IP4               = ["10.162.101.248/29"]
crmmis_api_key                  = "ed7cebc2-510e-4e4e-8428-c6b09596cfea"
instrumentorderform_subnet_IP4  = ["10.162.101.240/29"]
